/*
注意事項
グローバル変数はこれ以上は増やさずに作業すること
もしどうしても必要な場合は、必ずどこかに明記しておくこと
参考サイト：
クラスに関すること：https://qiita.com/jooex/items/981824f9fb494b448a08
canvasに関すること：https://qiita.com/kyrieleison/items/a3ebf7c55295c3e7d8f0
ドラッグ等の捜査に関すること：https://qiita.com/kyrieleison/items/26fcfb3a1d94bc92c596
配列に関すること：https://qiita.com/takeharu/items/d75f96f81ff83680013f
*/

var canvas_1 = document.getElementById("canvas_1"); //本棚用のキャンバスidは"canvas_1"と定義する
var ctx = canvas_1.getContext('2d');

class Bookshelf {
  

  constructor(id, posX, posY, book_id = null) {
    //本棚の長さは自由に変えても大丈夫
    this.lengthX = 20; //描画する本棚のx成分の長さ
    this.lengthY = 15; //描画する本棚のy成分の長さ
    this.dragging = false;
    this.id = id;
    this.posX = posX;
    this.posY = posY;
    this.book_id = book_id;
    this.relX=0; //オブジェクトとマウス座標との差分
    this.relY=0; //オブジェクトとマウス座標との差分
    this.draw(); //生成と同時に描画する
    
  }


  // draw(x = this.posX, y = this.posY) {
    draw(x, y ) {
    /*
    仕様：canvas上の(x,y)の位置に四角形、大きさはlengthX,lengthYで描画する、を描画する
    備考：一番最初の処理に動かしたときのことを考慮して、図形を消す処理を入れる
    */
    
   
    ctx.clearRect(this.posX, this.posY, this.lengthX, this.lengthY); 
    if(this.dragging){
      ctx.fillRect(x+this.relX,y+this.relY,this.lengthX,this.lengthY);
      this.posX=x+this.relX;
      this.posY=y+this.relY;
    } else {
      ctx.fillRect(this.posX,this.posY,this.lengthX,this.lengthY);
    }
    
  }

  on_mouse(e) {
    /*
    ＊ちなみにこの引数eはマウスの情報が格納されてる
    マウスの座標はe.clientX,e.clientYで取得できる
  
    仕様：マウスの座標が描画されたこのクラス上にあるかを判定する
    マウスの座標が描画されたこのクラス上にある場合はdraggingをtrueにする
    */

    console.log("おんまうす");
    var offsetX = canvas_1.getBoundingClientRect().left;
    var offsetY = canvas_1.getBoundingClientRect().top;
    var x=e.clientX-offsetX;
    var y=e.clientY-offsetY;

    if (this.posX < x
      && (this.posX + this.lengthX) > x 
      && this.posY < y
      && (this.posY + this.lengthY) > y) 
    {
      this.dragging = true; // ドラッグ開始
      this.relX = this.posX - x;
      this.relY = this.posY - y;
    }
    console.log(this.posX);
    console.log(x);
    console.log(this.dragging);
  }

  drag(e) {
    console.log("むーぶまうす");
    /*
    仕様：draggingを判定して、trueが帰ってきた場合ドラッグした位置にdraw()メソッドで描画する
    返り値：なし
    備考：可能なら他の本棚とぶつかった際は被らないようにする。
    */

    var offsetX = canvas_1.getBoundingClientRect().left;
    var offsetY = canvas_1.getBoundingClientRect().top;
    var newX=e.clientX-offsetX;
    var newY=e.clientY-offsetY;

    if(this.dragging){
       this.draw(newX,newY);
    }
  }

  up_mouse(e){
    console.log("あっぷまうす");
    this.dragging = false;
  }
}

var bookshelf_list = []; //本棚を格納するための変数

//この下は関数
function bookshelf_init() {
  /*
  DBからの情報をbookshelfに格納するための関数
  まだ、触らない
  */
}

function create_bookshelf() {
  console.log("OK");
  /*
  仕様：htmlのinputがクリックされた時に呼び出す関数
  bookshelfクラスを宣言して、インスタンスをbookshelf_listの末尾に格納する
  宣言する際のidは0から順番につける(実装方法はbookshelf_listの長さを利用する)
  同上のbook_idはnull値を指定する
  インスタンスのイベントをcanvas_1.addEventList()で追加する(on_mouse()を'mousedown'、drag()を'mousemove'、up_mouse()を'mouseup'で追加)
  */

  var num = bookshelf_list.length;
  bookshelf_list.push(new Bookshelf(num,80+num,80+num,null))
  canvas_1.addEventListener('mousedown', bookshelf_list[num].on_mouse.bind(bookshelf_list[num]), false);
  canvas_1.addEventListener('mousemove', bookshelf_list[num].drag.bind(bookshelf_list[num]), false);
  canvas_1.addEventListener('mouseup', bookshelf_list[num].up_mouse.bind(bookshelf_list[num]), false);
}