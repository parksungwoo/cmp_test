<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>book_info</title>
        <link rel="stylesheet" href="CSS/book_style.css">
        <?php
        $title = $_GET["title"];
        $author = $_GET["author"];
        $publish = $_GET["publish"];
        $description = $_GET["description"];
        ?>
    </head>
    <body>
        <a href="top.html">戻る</a><br>
        <canvas></canvas>
        <span></span>
        <table>
            <tr>
                <td id="author">作者：</td>
                <script>
                    var input = document.getElementById("author");
                    input.innerText = <?php echo '"' . $author . '"'; ?>
                </script>
            </tr>
            <tr>
                <td id="title">タイトル：</td>
                <script>
                    var input = document.getElementById("title");
                    input.innerText += <?php echo '"' . $title . '"'; ?>
                </script>
            </tr>
            <tr>
                <td id="publish">出版日：</td>
                <script>
                    var input = document.getElementById("publish");
                    input.innerText += <?php echo '"' . $publish . '"'; ?>
                </script>
            </tr>
            <tr>
                <td id="description">書籍説明：</td>
                <script>
                    var input = document.getElementById("description");
                    input.innerText += <?php echo '"' . $description . '"'; ?>
                </script>
            </tr>
        </table><br>
        <a href="top.php">ホームヘ</a>
    </body>
</html>