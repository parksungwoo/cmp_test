<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>top</title>
    <link rel="stylesheet" href="CSS/book_style.css">
</head>

<body>
    <CENTER>
    <h1>topページ</h1>
    <input type="button" value="本棚ページ" onClick="location.href='book_shelf.html'">
    <input type="button" value="書籍登録ページ" onClick="location.href='book_registor.html'"
    <br>
    <form action="book_search_result.php" method="get">
        書籍検索
        <input type="text" name="book_search"> <br>
        タグ検索
        <input type="text" name="tag_search"><br>
        <input type="submit" value="検索">
    </form>
    <span id="news_feed">新着書籍</span>
    <?php
    $pdo = new PDO('sqlite:SQL/bookdata.sqlite');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $info = $pdo->prepare("SELECT * FROM add_information where ID >= ?");
    $info->execute([0]);
    echo '<table>';
    echo '<tr>';
    echo '<td>日付</td>';
    echo '<td>タイトル</td>';
    echo '<td>作者</td>';
    echo '<td>本棚番号</td>';
    echo '</tr>';
    foreach ($info as $i) {
        echo '<tr>';
        echo '<td>' . htmlspecialchars($i['day']) . '</td>';
        echo '<td>' . htmlspecialchars($i['title']) . '</td>';
        echo '<td>' . htmlspecialchars($i['author']) . '</td>';
        echo '<td>' . htmlspecialchars($i['bookshelfID']) . '</td>';
        echo '</tr>';
    }
    echo '</table>';
    ?>
    </CENTER>
</body>

</html>